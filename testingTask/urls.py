from django.urls import path
from django.conf.urls import include
from django.contrib import admin
from django.views.generic import RedirectView

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path(r'application/', include('application.urls')),
    # redirect as we are not going to have another projects
    path('', RedirectView.as_view(url='/application/', permanent=True)),
    path(r'admin/', admin.site.urls),
    path(r'accounts/', include('django.contrib.auth.urls')),
]
