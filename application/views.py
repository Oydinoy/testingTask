from django.shortcuts import render
from .models import Course, Material, UserCourseBind
from django.views import generic, View
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.decorators import permission_required, login_required


def index(request):
    num_courses = Course.objects.all().count()

    return render(
        request,
        'index.html',
        context={'num_courses': num_courses}
    )


def CoursesListView(request):
    return render(
        request,
        'courses_list.html',
        context={'courses_list': Course.objects.all()}
    )


class CourseDetailView(generic.DetailView):
    model = Course
    template_name = 'course_detail.html'


class SubscribedCoursesByUserListView(LoginRequiredMixin, generic.ListView):

    model = UserCourseBind
    template_name ='subsribed_courses_of_user.html'

    class Meta:
        permissions = "can_see_materials"

    def get_queryset(self):
        return UserCourseBind.objects.filter(user=self.request.user)
