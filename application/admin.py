from django.contrib import admin

from .models import Course, Material, UserCourseBind

admin.site.register(Course)
admin.site.register(Material)

