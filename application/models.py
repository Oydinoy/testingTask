from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User


class Course(models.Model):
    """
    Course class definition
    """

    name = models.CharField(unique=True, max_length=255, help_text='Enter the course name. Remember, it should '
                                                                   'be unique.')
    description = models.CharField(null=True, blank=True, help_text="Enter the description of the course",
                                   max_length=255)
    rating = models.CharField(blank=True, max_length=7, default='0')

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('model-detail-view', args=[str(self.id)])


class Material(models.Model):
    """
    Material of a course description
    """

    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name="material_course")
    pdf_file = models.FileField(null=True, blank=True, help_text='Add a pdf file for this course material')
    texts = models.CharField(blank=True, max_length=2047, help_text='Enter the material\'s text')
    web_urls = models.CharField(blank=True, max_length=255, help_text='Add a connected url')

    class Meta:
        ordering = ['course']

    def __str__(self):
        return self.texts


class UserCourseBind(models.Model):
    """
    Bind between User and Courses
    """

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_related")
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name="course_related")
    rating = models.IntegerField(blank=True)
    comment = models.CharField(null=True, blank=True, max_length=255)

    def __str__(self):
        return self.name
