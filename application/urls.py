from django.urls import path
from . import views
from django.conf.urls import url

urlpatterns = [
    url(r'^courses/$', views.CoursesListView, name='courses'),
    url(r'^courses/(?P<pk>\d+)$', views.CourseDetailView.as_view(), name='course-detail'),
    # url(r'^courses/(?P<pk>\d+)$/material', views.MaterialView, name='view-materials'),
    url(r'^mycourses/$', views.SubscribedCoursesByUserListView.as_view(), name='my-courses'),
    url('', views.index, name='index'),
]