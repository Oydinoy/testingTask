# Generated by Django 3.2 on 2021-12-10 10:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('application', '0002_adding_additional_info_to_model'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='name',
            field=models.CharField(help_text='Enter the course name. Remember, it should be unique.', max_length=255, unique=True),
        ),
    ]
